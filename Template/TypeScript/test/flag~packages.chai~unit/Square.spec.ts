import {should} from 'chai';
import {Square} from "../../src/lib/Square";

should();


describe("Square", () => {
    const square1 = new Square(5);
    const square2 = new Square(10);

    it("should compute area based on width", () => {
        square1.Area.should.equal(25);
        square2.Area.should.equal(100);
    });

    it("should compute perimeter based on width", () => {
        square1.Perimeter.should.equal(20);
        square2.Perimeter.should.equal(40);
    });

});
