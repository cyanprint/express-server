// @ts-ignore
import {Square} from "../../src/lib/Square";

const square = new Square(5);

test("Square Area", () => {

    expect(square.Area).toBe(25)

});

test("Square Perimeter", () => {
    expect(square.Perimeter).toBe(20)
});