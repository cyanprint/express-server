import express, {Application, Request, Response} from "express"
import bodyParser from "body-parser"; //flag~packages.body-parser~
import {PhotoController} from "./controller/flag~packages.typeorm~flag!~swagger~photo";
import {createConnections, getConnection} from "typeorm"; //flag~packages.typeorm~
import {Postgres} from "./flag~packages.typeorm~flag~db.postgres~postgres";
import {MySQL} from "./flag~packages.typeorm~flag~db.mysql~mysql";
import {MariaDB} from "./flag~packages.typeorm~flag~db.maria~maria";
import {SQLite} from "./flag~packages.typeorm~flag~db.sqlite~sqlite";
import {RegisterRoutes} from "./routes"; //flag~swagger~
import swaggerUi from "swagger-ui-express" //flag~swagger~
import "./controller/flag~swagger~book";
import "./controller/flag~packages.typeorm~flag~swagger~photo";

const swagger = require("./swagger.json"); //flag~swagger~

const PORT = process.env.PORT || 3000;


export async function Main(argv: string[]) {

    //if~packages.typeorm~
    await createConnections([
        Postgres(), //flag~db.postgres~
        MySQL(), //flag~db.mysql~
        MariaDB(), //flag~db.maria~
        SQLite(), //flag~db.sqlite~
    ]);
    //end~packages.typeorm~


    const app: Application = express();

    app.use(bodyParser.json()); //flag~packages.body-parser~

    // Basic routes
    app.get("/", (req: Request, res: Response) => {
        res.send("Hello World");
    });
    app.post("/", (req: Request, res: Response) => {
        // get body
        console.log(req.body);

        //send response
        res.send(req.body);
    });

    // if~swagger~
    RegisterRoutes(app as express.Express);
    app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swagger));
    // end~swagger~

    // if~packages.typeorm~
    // if!~swagger~
    PhotoController(app, 'postgres-db', getConnection("postgres-db")); //flag~db.postgres~
    PhotoController(app, 'mysql-db', getConnection("mysql-db")); //flag~db.mysql~
    PhotoController(app, 'mariadb-db', getConnection("mariadb-db")); //flag~db.maria~
    PhotoController(app, 'sqlite-db', getConnection("sqlite-db")); //flag~db.sqlite~
    // end!~swagger~

    // end~packages.typeorm~

    app.listen(PORT, () => console.log(`listening on ${PORT}`));


}

