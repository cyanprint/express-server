import {Photo} from "./entity/flag~packages.typeorm~photo";
import {SqliteConnectionOptions} from "typeorm/driver/sqlite/SqliteConnectionOptions";


export function SQLite(): SqliteConnectionOptions {

    return {
        name: "sqlite-db",
        type: "sqlite",
        database: process.env.SQLITE_NAME as string,
        entities: [
            Photo
        ],
        synchronize: true,
        logging: false
    };
}
