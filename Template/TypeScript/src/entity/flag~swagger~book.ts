export class Book {
    id: string;
    author: string;
    description: string;
}