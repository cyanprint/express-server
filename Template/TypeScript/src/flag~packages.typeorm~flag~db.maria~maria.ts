import "reflect-metadata";
import {Photo} from "./entity/flag~packages.typeorm~photo";
import {MysqlConnectionOptions} from "typeorm/driver/mysql/MysqlConnectionOptions";

export function MariaDB(): MysqlConnectionOptions {

    const port = parseInt(process.env.MARIA_PORT || "3306");

    return {
        name: "mariadb-db",
        type: "mariadb",
        host: process.env.MARIA_HOST || "localhost",
        port,
        username: process.env.MARIA_USER,
        password: process.env.MARIA_PASSWORD,
        database: process.env.MARIA_NAME,
        entities: [
            Photo
        ],
        synchronize: true,
        logging: false
    }

}

