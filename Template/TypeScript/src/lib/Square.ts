class Square {
    width: number;

    constructor(width: number) {
        this.width = width;
    }


    get Area(): number {
        return this.width ** 2;
    }

    get Perimeter(): number {
        return this.width * 4;
    }


}

export {Square}