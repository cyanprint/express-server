import {Photo} from "./entity/flag~packages.typeorm~photo";
import {PostgresConnectionOptions} from "typeorm/driver/postgres/PostgresConnectionOptions";


export function Postgres(): PostgresConnectionOptions {
    const port = parseInt(process.env.POSTGRES_PORT || "5432");
    return {
        name: "postgres-db",
        type: "postgres",
        host: process.env.POSTGRES_HOST,
        port,
        username: process.env.POSTGRES_USER,
        password: process.env.POSTGRES_PASSWORD,
        database: process.env.POSTGRES_NAME,
        entities: [
            Photo
        ],
        synchronize: true,
        logging: false
    };
}