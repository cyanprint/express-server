import {Application, Request, Response} from "express";
import {Connection} from "typeorm";
import {Photo} from "../entity/flag~packages.typeorm~photo";

export function PhotoController(app: Application, path: string, con: Connection) {
    const repository = con.getRepository(Photo);
    app.get(`/photo/${path}`, async (req: Request, res: Response) => {
        const photos = await repository.find();
        res.send(photos);
    });

    app.get(`/photo/${path}/:name`, async (req: Request, res: Response) => {
        const name = req.params["name"];
        const photo = await repository.findOne({name});
        if (photo == null) {
            res.status(404);
            res.send();
        } else {
            photo.views++;
            const ret = await repository.save(photo);
            res.send(ret)
        }
    });

    app.post(`/photo/${path}/:name`, async (req: Request, res: Response) => {
        const name = req.params["name"];
        const photo = new Photo();
        photo.name = name;
        photo.description = "A random photo";
        photo.filename = "random-photo.png";
        photo.isPublished = true;
        photo.views = 0;
        const saved = await repository.save(photo);
        res.status(201);
        res.send(saved);
    });

    app.delete(`/photo/${path}/:name`, async (req: Request, res: Response) => {
        const name = req.params["name"];
        const photo = await repository.findOne({name});
        if (photo == null) {
            res.status(404);
            res.send();
        } else {
            await repository.remove(photo);
            res.status(204);
            res.send();
        }
    });

}