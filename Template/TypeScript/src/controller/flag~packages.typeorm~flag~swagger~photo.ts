import {Controller, Delete, Get, Post, Route, SuccessResponse} from "tsoa";
import {Photo} from "../entity/flag~packages.typeorm~photo";
import {getConnection} from "typeorm";

@Route('photo')
export class PhotoController extends Controller {

    db: string;

    // GET /v1/photo/:db
    @Get('{db}')
    public async GetAll(db: string): Promise<Photo[]> {
        const repo = getConnection(db).getRepository(Photo);
        return repo.find()
    }

    // GET /v1/photo/:db/:name
    @Get('{db}/{name}')
    public async GetBook(db: string, name: string): Promise<Photo> {
        const repo = getConnection(db).getRepository(Photo);
        const photo = await repo.findOne({name});
        if (photo == null) {
            this.setStatus(404);
        }
        return photo!;
    }

    // DELETE /v1/photo/:db/:name
    @SuccessResponse('204', 'NoContent')
    @Delete('{db}/{name}')
    public async DeleteBook(db: string, name: string): Promise<void> {
        const repo = getConnection(db).getRepository(Photo);
        const photo = await repo.findOne({name});
        if (photo == null) {
            this.setStatus(404);
            return
        }
        this.setStatus(204);
        return
    }

    // POST /v1/photo/:db/:name
    @SuccessResponse('201', 'Created')
    @Post('{db}/{name}')
    public async CreatUser(db: string, name: string): Promise<Photo> {
        const repo = getConnection(db).getRepository(Photo);
        const photo = new Photo();
        photo.name = name;
        photo.description = "A random photo";
        photo.filename = "random-photo.png";
        photo.isPublished = true;
        photo.views = 0;
        const saved = await repo.save(photo);
        this.setStatus(201);
        return saved;
    }

}