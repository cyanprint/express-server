import {Body, Controller, Delete, Get, Post, Route, SuccessResponse} from "tsoa";
import {Book} from "../entity/flag~swagger~book";

@Route('Book')
export class BookController extends Controller {

    private static books: { [s: string]: Book } = {};

    // GET /v1/Book
    @Get()
    public GetAll(): { [s: string]: Book } {
        return BookController.books;
    }

    // GET /v1/Book/:id
    @Get('{id}')
    public GetBook(id: string): Book {
        const book = BookController.books[id];
        if (book == null) {
            this.setStatus(404);
        }
        return book;
    }

    // DELETE /v1/Book/:id
    @SuccessResponse('204', 'NoContent')
    @Delete('{id}')
    public DeleteBook(id: string) {
        if (BookController.books == null) {
            this.setStatus(404);
            return
        }
        delete BookController.books[id];
        this.setStatus(204);
    }

    // POST /v1/Book/:id
    @SuccessResponse('201', 'Created') // Custom success response
    @Post('{id}')
    public CreatUser(id: string, @Body() requestBody: Book) {
        requestBody.id = id;
        BookController.books[id] = requestBody;
        this.setStatus(201);
    }

}