import {Main} from "./app";
//if~packages.@kirinnee/core~
import {Core, Kore} from "@kirinnee/core";

const core: Core = new Kore();
core.ExtendPrimitives();
//end~packages.@kirinnee/core~


try {
    Main(process.argv.slice(2)).then();
} catch (e) {
    console.log(e);
}
