import "reflect-metadata";
import {Photo} from "./entity/flag~packages.typeorm~photo";
import {MysqlConnectionOptions} from "typeorm/driver/mysql/MysqlConnectionOptions";


export function MySQL(): MysqlConnectionOptions {

    const port = parseInt(process.env.MYSQL_PORT || "3306");
    return {
        name: "mysql-db",
        type: "mysql",
        host: process.env.MYSQL_HOST || "localhost",
        port,
        username: process.env.MYSQL_USER,
        password: process.env.MYSQL_PASSWORD,
        database: process.env.MYSQL_NAME,
        entities: [
            Photo
        ],
        synchronize: true,
        logging: false
    };

}

