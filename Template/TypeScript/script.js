const spawn = require('child_process').spawn;
const rimraf = require("rimraf"); //flag~packages.nyc~
const fs = require("fs");
const del = require('del');

//command will be the first argument
const command = process.argv[2];
//args will be the array of arguments after the commands
const args = process.argv.slice(3).map(s => s.substr(2) === "--" ? s.substr(1) : s).filter(s => s !== '-w' && s !== '-watch');
//This is a boolean as to whether --watch or -w been in the commands
const watch = process.argv.map(s => s.substr(2) === "--" ? s.substr(1) : s).filter(s => s === '-w' || s === "-watch").length > 0;

Execute(command, args, watch).then();


//The decision on what command to execute base on the arguments and whether watch exists
async function Execute(command, args, watch) {
    let coverage = args.filter(s => s === "--cover").length > 0; //flag~cover~
    let dotenv = args.filter(s => s === "--dotenv").length > 0; //flag~packages.dotenv~

    function Clean() {
        try {
            del.sync(['dist/**/*.*', 'dist/*', '!dist/.gitignore', '!dist/.dockerignore']);
        } catch {

        }
    }

    //if~swagger~
    async function GenerateTSOA() {
        await run("tsoa -c ./config/flag~swagger~tsoa.json routes");
        const content = fs.readFileSync("./src/routes.ts", 'utf8');
        fs.writeFileSync("./src/routes.ts", "//@ts-nocheck\n" + content, 'utf8');
        await run("tsoa -c ./config/flag~swagger~tsoa.json swagger");
        fs.copyFileSync("./src/swagger.json", "./dist/swagger.json");
    }

    //end~swagger~

    switch (command) {

        case "dev":
            await GenerateTSOA(); //flag~swagger~
            let dev = "ts-node ";
            dev += "-r dotenv/config "; //flag~packages.dotenv~
            dev += "src/startup.ts";
            if (watch) dev = "nodemon --exec " + dev; //flag~packages.nodemon~
            await run(dev);
            break;
        case "clean":
            Clean();
            break;
        case "build":
            Clean();
            await GenerateTSOA(); //flag~swagger~
            try {
                await run("tsc", true)
            } catch {
            }
            break;
        case "prod" :
            Clean();
            await GenerateTSOA(); //flag~swagger~
            try {
                await run("tsc", true)
            } catch {
            }
            let start = "node ";
            if (dotenv) start += "-r dotenv/config "; //flag~packages.dotenv~
            start += "dist/startup.js";
            await run(start);
            break;
        //if~packages.chai~
        case "flag~packages.chai~unit":
            let mocha = `mocha --config ./config/flag~packages.chai~.mocharc.js`;
            if (watch) mocha += " --watch-extensions ts --watch ";
            if (coverage) mocha = "nyc --nycrc-path ./config/flag~packages.nyc~.nycrc " + mocha; //flag~cover~
            if (fs.existsSync("./.nyc_output")) rimraf.sync("./.nyc_output"); //flag~cover~
            await run(mocha);
            break;
        //end~packages.chai~
        //if~packages.jest~
        case "flag~packages.jest~unit":
            let jest = `jest --rootDir="." --config=./config/flag~packages.jest~jest.config.js ./test/flag~packages.jest~unit`;
            if (watch) jest += " --watch";
            if (coverage) jest += " --coverage";  //flag~cover~
            await run(jest);
            break;
        //end~packages.jest~
        default:
            console.log("Unknown command!");
            process.exit(1);
    }

}

//Executes the function as if its on the CMD. Exits the script if the external command crashes.
async function run(command, silent) {
    silent = silent == null ? false : silent;
    console.log(command);
    if (!Array.isArray(command) && typeof command === "string") command = command.split(' ');
    else throw new Error("command is either a string or a string array");
    let c = command.shift();
    let v = command;

    return new Promise((resolve, reject) => spawn(c, v,
        {
            stdio: "inherit",
            shell: true,
        })
        .on("exit", (code, signal) => {
            if (fs.existsSync("./.nyc_output")) rimraf.sync("./.nyc_output"); //flag~cover~
            if (code === 0) resolve();
            else {
                console.log("ExternalError:", signal);
                if (!silent) {
                    process.exit(1);
                } else {
                    reject(signal);
                }
            }
        })
    );
}