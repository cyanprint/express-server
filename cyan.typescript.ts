import {Chalk} from "chalk";
import {Inquirer} from "inquirer";
import {Cyan, Documentation, DocUsage, Glob, IAutoInquire, IAutoMapper, IExecute} from "./Typings";

export = async function (folderName: string, chalk: Chalk, inquirer: Inquirer, autoInquirer: IAutoInquire, autoMap: IAutoMapper, execute: IExecute): Promise<Cyan> {

    let useWebStorm = await autoInquirer.InquirePredicate("Do you use WebStorm?");

    let globs: Glob[] = [{root: "./Template/Common/", pattern: "**/*.*", ignore: []}];
    let guid: string[] = [];
    let NPM: string | boolean = true;

    let basicFolder: Glob = {root: "./Template/Typescript", pattern: ["**/*.*", "**/*"], ignore: []};
    if (!useWebStorm) {
        (basicFolder.ignore as string[]).push("**/.idea/**/*");
        (basicFolder.ignore as string[]).push("**/.idea/**/*.*");
    }
    globs.push(basicFolder);


    //Ask for documentation
    let docQuestions: DocUsage = {license: false, git: false, readme: true, semVer: false, contributing: false};
    let docs: Documentation = await autoInquirer.InquireDocument(docQuestions);

    //Get read question to ask
    let variables: any = {
        name: {
            package: ["vue-frontend", "Enter the name of package.json"]
        },
        color: {},
    };

    //Get answers
    variables = await autoInquirer.InquireInput(variables);

    //Fill in empty information if document already have them
    variables.name.author = docs.data.author;
    variables.email = docs.data.email;
    variables.description = docs.data.description;


    let flags: any = {
        db: {
            postgres: false,
            mysql: false,
            maria: false,
            sqlite: false
        },
        unitTest: false,
        swagger: false,
        cover: false,
        docker: false,
        packages: {

            //Type ORM
            typeorm: false,

            //dotenv
            dotenv: false,

            //body-parser
            "body-parser": false,

            //dbs
            mysql2: false,
            pg: false,
            sqlite3: false,

            //test
            chai: false,
            mocha: false,
            jest: false,
            nyc: false,
            rimraf: false,

            //TSOA
            "swagger-ui-express": false,
            tsoa: false,


            // Refresh
            nodemon: false,


            //Typescript stuff
            "@types/jest": false,
            "@types/chai": false,
            "@types/mocha": false,
            "ts-jest": false,

            "@types/body-parser": false,
            "@types/swagger-ui-express": false,
            "@kirinnee/core": false

        },
    };


    //Ask for for features
    let features: any = {
        unitTest: "Unit Test",
        swagger: "TSOA (swagger)",
        docker: "Docker",
        packages: {
            typeorm: "Type ORM (SQL Databases)",
            nodemon: "nodemon (auto-reload)",
            dotenv: ".env",
            "@kirinnee/core": "Kirinnee Core Library"
        },
    };

    features = await autoInquirer.InquireAsCheckBox(features, "Which of these tools do you need?");
    flags = autoMap.Overwrite(features, flags);

    if (!flags.packages.dotenv) {
        (basicFolder.ignore as string[]).push("**/.env");
    }

    if (!flags.docker) {
        (basicFolder.ignore as string[]).push("**/.dockerignore");
    }


    //if unit test
    if (flags.unitTest) {

        let coverage = await autoInquirer.InquirePredicate("Do you need coverage tool?");
        flags.cover = coverage;

        let mochaChai: any = {
            packages: {
                chai: true,
                mocha: true,
                nyc: coverage,
                rimraf: coverage,
            },
        };
        let jest: any = {
            packages: {
                jest: true
            },
        };
        let test: boolean = await autoInquirer.InquirePredicate("Which Test Framework do you want to use?", "Mocha + Chai", "Jest");
        flags = autoMap.Overwrite(test ? mochaChai : jest, flags);

    }
    delete flags.unitTest;

    if (flags.packages.jest) {
        flags.packages["ts-jest"] = true;
        flags.packages["@types/jest"] = true;
    }


    if (flags.packages.chai) {
        flags.packages["@types/chai"] = true;
        flags.packages["@types/mocha"] = true;
    }

    if (flags.swagger) {
        flags.packages.tsoa = true;
        flags.packages["swagger-ui-express"] = true;
        flags.packages["@types/swagger-ui-express"] = true;
        flags.packages["body-parser"] = true;
    } else {
        flags.packages["body-parser"] = await autoInquirer.InquirePredicate("Do you want to use body-parser?")
    }

    if (flags.packages.typeorm) {
        flags.db = await autoInquirer.InquireAsCheckBox({
            postgres: "PostgreSQL",
            mysql: "MySQL",
            maria: "MariaDB",
            sqlite: "SQLite3",
        }, "Which database do you need?");
        if (flags.db.maria || flags.db.mysql) {
            flags.packages.mysql2 = true;
        }
        if (flags.db.sqlite) {
            flags.packages.sqlite3 = true;
        } else {
            (basicFolder.ignore as string[]).push("**/database.db")
        }
        if (flags.db.postgres) {
            flags.packages.pg = true;
        }
    }

    if (flags.docker && flags.packages.typeorm) {
        flags.depends = flags.db.mysql || flags.db.maria || flags.db.postgres;
    }

    if (docs.usage.git) variables.git = docs.data.gitURL;


    return {
        globs,
        flags,
        variable: variables,
        docs,
        guid,
        comments: ["//", "#"],
        npm: NPM
    } as Cyan;
}
