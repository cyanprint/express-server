import {Chalk} from "chalk";
import {Inquirer} from "inquirer";
import {Cyan, IAutoInquire, IAutoMapper, IExecute,} from "./Typings";

export = async function (folderName: string, chalk: Chalk, inquirer: Inquirer, autoInquirer: IAutoInquire, autoMap: IAutoMapper, execute: IExecute): Promise<Cyan> {
    let isTypescript = await autoInquirer.InquirePredicate("What language do you write in?", "Typescript", "Javascript");
    let subConfig: string = isTypescript ? "./cyan.typescript.js" : "./cyan.javascript.js";
    return execute.call(subConfig);
}